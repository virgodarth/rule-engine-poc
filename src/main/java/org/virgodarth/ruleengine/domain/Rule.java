package org.virgodarth.ruleengine.domain;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Entity
@Getter
@Setter
@Table(
    name = "rule",
    uniqueConstraints =
        @UniqueConstraint(
            name = "UC_Rule_CodeAndNamespace",
            columnNames = {"code", "namespace"}))
public class Rule {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id")
  private Long id;

  @Column(name = "code")
  private String code;

  @Column(name = "namespace")
  private String namespace;

  @Column(name = "condition")
  private String condition;

  @Column(name = "action")
  private String action;

  @Column(name = "priority")
  private Integer priority;

  @Column(name = "description")
  private String description;
}
