package org.virgodarth.ruleengine.facade;

import java.util.List;

import org.virgodarth.ruleengine.dto.RuleDto;
import org.virgodarth.ruleengine.facade.ruleengine.impl.loan.LoanDetails;
import org.virgodarth.ruleengine.facade.ruleengine.impl.loan.UserDetails;

public interface RuleFacade {
  List<RuleDto> getRulesByNamespace(String namespace);

  LoanDetails evaluate(String namespace, UserDetails userDetails);
}
