package org.virgodarth.ruleengine.facade.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;
import org.virgodarth.ruleengine.dto.RuleDto;
import org.virgodarth.ruleengine.facade.RuleFacade;
import org.virgodarth.ruleengine.facade.mapper.RuleMapper;
import org.virgodarth.ruleengine.facade.ruleengine.RuleEngine;
import org.virgodarth.ruleengine.facade.ruleengine.impl.loan.LoanDetails;
import org.virgodarth.ruleengine.facade.ruleengine.impl.loan.LoanInferenceRule;
import org.virgodarth.ruleengine.facade.ruleengine.impl.loan.UserDetails;
import org.virgodarth.ruleengine.service.RuleService;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class RuleFacadeImpl implements RuleFacade {
  private final RuleService ruleService;

  private final LoanInferenceRule loanInferenceRule;
  private final RuleEngine ruleEngine;

  private final RuleMapper ruleMapper;

  @Override
  public List<RuleDto> getRulesByNamespace(String namespace) {
    return ruleService.getRulesByName(namespace).stream()
        .map(ruleMapper::ruleToRuleDto)
        .collect(Collectors.toList());
  }

  @Override
  public LoanDetails evaluate(String namespace, UserDetails userDetails) {
    return (LoanDetails) ruleEngine.run(loanInferenceRule, userDetails);
  }
}
