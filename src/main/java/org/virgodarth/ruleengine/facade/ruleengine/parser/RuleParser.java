package org.virgodarth.ruleengine.facade.ruleengine.parser;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class RuleParser<INPUT_DATA, OUTPUT_RESULT> {

  private final DslParser dslParser;
  private final MvelParser mvelParser;

  private final String INPUT_KEYWORD = "input";
  private final String OUTPUT_KEYWORD = "output";

  /**
   * Parsing in given priority/steps.
   *
   * <p>Step 1. Resolve domain specific keywords first: $(rule_namespace.keyword) Step 2. Resolve
   * MVEL expression.
   *
   * @param expression
   * @param inputData
   */
  public boolean parseCondition(String expression, INPUT_DATA inputData) {
    String resolvedDslExpression = dslParser.resolveDomainSpecificKeywords(expression);
    
    Map<String, Object> input = new HashMap<>();
    input.put(INPUT_KEYWORD, inputData);
    
    return mvelParser.parseMvelExpression(resolvedDslExpression, input);
  }

  /**
   * Parsing in given priority/steps.
   *
   * <p>Step 1. Resolve domain specific keywords: $(rule_namespace.keyword) Step 2. Resolve MVEL
   * expression.
   *
   * @param expression
   * @param inputData
   * @param outputResult
   * @return
   */
  public OUTPUT_RESULT parseAction(
      String expression, INPUT_DATA inputData, OUTPUT_RESULT outputResult) {
    String resolvedDslExpression = dslParser.resolveDomainSpecificKeywords(expression);
    Map<String, Object> input = new HashMap<>();
    input.put(INPUT_KEYWORD, inputData);
    input.put(OUTPUT_KEYWORD, outputResult);
    mvelParser.parseMvelExpression(resolvedDslExpression, input);
    return outputResult;
  }
}
