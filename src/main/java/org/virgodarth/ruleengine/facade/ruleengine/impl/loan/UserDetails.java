package org.virgodarth.ruleengine.facade.ruleengine.impl.loan;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserDetails {
  String firstName;
  String lastName;
  Integer age;
  Long accountNumber;
  Double monthlySalary;
  String bank;
  Integer creditScore;
  Double requestedLoanAmount;
  
  public boolean greaterThanAge(int n){
    return age < n;
  }
  
//  public int countStudentByScore(int sccore){
//    return scores.count(x < sccore)
//  }
}
