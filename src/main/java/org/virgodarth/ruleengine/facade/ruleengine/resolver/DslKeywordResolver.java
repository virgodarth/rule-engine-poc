package org.virgodarth.ruleengine.facade.ruleengine.resolver;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

@Service
public class DslKeywordResolver {
  private final Map<String, DslResolver> dslKeywordResolverByName;

  public DslKeywordResolver(final List<DslResolver> resolvers) {
    dslKeywordResolverByName =
        resolvers.stream()
            .collect(Collectors.toMap(DslResolver::getResolverKeyword, Function.identity()));
  }

  public Optional<DslResolver> getResolver(String keyword) {
    return Optional.ofNullable(dslKeywordResolverByName.get(keyword));
  }
}
