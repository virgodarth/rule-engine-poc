package org.virgodarth.ruleengine.facade.ruleengine;

import org.springframework.stereotype.Service;
import org.virgodarth.ruleengine.service.RuleService;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class RuleEngine {
  private final RuleService ruleService;

  public Object run(InferenceEngine inferenceEngine, Object inputData) {
    String ruleNamespace = inferenceEngine.getRuleNamespace().toString();
    // TODO: Here for each call, we are fetching all rules from db. It should be cache.
    final var rules = ruleService.getRulesByName(ruleNamespace);
    return inferenceEngine.run(rules, inputData);
  }
}
