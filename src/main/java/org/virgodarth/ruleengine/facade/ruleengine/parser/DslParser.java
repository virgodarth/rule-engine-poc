package org.virgodarth.ruleengine.facade.ruleengine.parser;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.virgodarth.ruleengine.facade.ruleengine.resolver.DslKeywordResolver;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class DslParser {
  private final DslKeywordResolver keywordResolver;
  private final DslPatternUtil dslPatternUtil;

  public String resolveDomainSpecificKeywords(String expression) {
    Map<String, Object> dslKeywordToResolverValueMap = executeDslResolver(expression);
    return replaceKeywordsWithValue(expression, dslKeywordToResolverValueMap);
  }

  private Map<String, Object> executeDslResolver(String expression) {
    final var keywords = dslPatternUtil.extractDslKeywords(expression);
    Map<String, Object> resolverValueByKeyword = new HashMap<>();
    keywords.forEach(
        dslKeyword -> {
          String extractedDslKeyword = dslPatternUtil.extractKeyword(dslKeyword);
          String keyResolver = dslPatternUtil.getKeywordResolver(extractedDslKeyword);
          String keywordValue = dslPatternUtil.getKeywordValue(extractedDslKeyword);

          final var resolver = keywordResolver.getResolver(keyResolver).get();
          final var resolveValue = resolver.resolveValue(keywordValue);

          resolverValueByKeyword.put(dslKeyword, resolveValue);
        });
    return resolverValueByKeyword;
  }

  private String replaceKeywordsWithValue(
      String expression, Map<String, Object> resolverValueByKeyword) {
    final var keys = resolverValueByKeyword.keySet();

    for (String key : keys) {
      String dslResolveValue = resolverValueByKeyword.get(key).toString();
      expression = expression.replace(key, dslResolveValue);
    }
    return expression;
  }
}
