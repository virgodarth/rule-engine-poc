package org.virgodarth.ruleengine.facade.ruleengine.impl.loan;

import org.springframework.stereotype.Service;
import org.virgodarth.ruleengine.enums.RuleNamespace;
import org.virgodarth.ruleengine.facade.ruleengine.InferenceEngine;
import org.virgodarth.ruleengine.facade.ruleengine.parser.RuleParser;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class LoanInferenceRule extends InferenceEngine<UserDetails, LoanDetails> {

  public LoanInferenceRule(RuleParser<UserDetails, LoanDetails> ruleParser) {
    super(ruleParser);
  }

  @Override
  protected RuleNamespace getRuleNamespace() {
    return RuleNamespace.LOAN;
  }

  @Override
  protected LoanDetails initializeOutputResult() {
    return new LoanDetails();
  }
}
