package org.virgodarth.ruleengine.facade.ruleengine.parser;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.springframework.stereotype.Component;
import org.virgodarth.ruleengine.common.constants.OperatorConstant;

import com.google.common.base.Splitter;
import com.google.common.collect.Lists;

@Component
public class DslPatternUtil {
  // $(rule_namespace.keyword)
  private static final Pattern DSL_PATTERN = Pattern.compile("\\$\\((\\w+)(\\.\\w+)\\)");

  public List<String> extractDslKeywords(String expression) {
    final var matcher = DSL_PATTERN.matcher(expression);
    var keywords = new ArrayList<String>();
    while (matcher.find()) {
      String group = matcher.group();
      keywords.add(group);
    }
    return keywords;
  }

  public String extractKeyword(String keyword) {
    return keyword.substring(keyword.indexOf('(') + 1, keyword.indexOf(')'));
  }

  public String getKeywordResolver(String keywords) {
    final var splitKeyword =
        Lists.newArrayList(Splitter.on(OperatorConstant.DOT).omitEmptyStrings().split(keywords));
    return splitKeyword.get(0);
  }

  public String getKeywordValue(String dslKeyword) {
    final var splitKeyword =
        Lists.newArrayList(Splitter.on(OperatorConstant.DOT).omitEmptyStrings().split(dslKeyword));
    return splitKeyword.get(1);
  }
}
