package org.virgodarth.ruleengine.facade.ruleengine;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.virgodarth.ruleengine.domain.Rule;
import org.virgodarth.ruleengine.enums.RuleNamespace;
import org.virgodarth.ruleengine.facade.ruleengine.parser.RuleParser;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public abstract class InferenceEngine<INPUT_DATA, OUTPUT_RESULT> {
  private final RuleParser<INPUT_DATA, OUTPUT_RESULT> ruleParser;

  /**
   * Run inference engine on set of rules for given data.
   *
   * @param rules
   * @param inputData
   * @return
   */
  public OUTPUT_RESULT run(List<Rule> rules, INPUT_DATA inputData) {
    if (null == rules || rules.isEmpty()) {
      return null;
    }

    // STEP 1 (MATCH) : Match the facts and data against the set of rules.
    final var matchingRules = match(rules, inputData);

    // STEP 2 (RESOLVE) : Resolve the conflict and give the selected one rule.
    final var resolvedRule = resolve(matchingRules);
    if (ObjectUtils.isEmpty(resolvedRule)) {
      return null;
    }

    // STEP 3 (EXECUTE) : Run the action of the selected rule on given data and return the output.
    return executeRule(resolvedRule, inputData);
  }

  /**
   * We can use here any pattern matching algo: 1. Rete 2. Linear 3. Treat 4. Leaps
   *
   * <p>Here we are using Linear matching algorithm for pattern matching.
   *
   * @param rules
   * @param inputData
   * @return
   */
  protected List<Rule> match(List<Rule> rules, INPUT_DATA inputData) {
    return rules.stream()
        .filter(rule -> ruleParser.parseCondition(rule.getCondition(), inputData))
        .collect(Collectors.toList());
  }

  /**
   * We can use here any resolving techniques: 1. Lex 2. Recency 3. MEA 4. Refactor 5. Priority wise
   *
   * <p>Here we are using find first rule logic.
   *
   * @param conflictSet
   * @return
   */
  protected Rule resolve(List<Rule> conflictSet) {
    return conflictSet.stream().findFirst().orElse(null);
  }

  /**
   * Execute selected rule on input data.
   *
   * @param rule
   * @param inputData
   * @return
   */
  protected OUTPUT_RESULT executeRule(Rule rule, INPUT_DATA inputData) {
    return ruleParser.parseAction(rule.getAction(), inputData, initializeOutputResult());
  }

  protected abstract OUTPUT_RESULT initializeOutputResult();

  protected abstract RuleNamespace getRuleNamespace();
}
