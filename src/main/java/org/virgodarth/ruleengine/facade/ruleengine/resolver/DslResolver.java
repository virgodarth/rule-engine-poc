package org.virgodarth.ruleengine.facade.ruleengine.resolver;

public interface DslResolver {
  String getResolverKeyword();

  Object resolveValue(String keyword);
}
