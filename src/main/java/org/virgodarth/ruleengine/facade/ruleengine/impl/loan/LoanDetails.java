package org.virgodarth.ruleengine.facade.ruleengine.impl.loan;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LoanDetails {
  Long accountNumber;
  Boolean approvalStatus;
  Float interestRate;
  Float sanctionedPercentage;
  Double processingFees;
}
