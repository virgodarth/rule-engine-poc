package org.virgodarth.ruleengine.facade.mapper;

import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.virgodarth.ruleengine.domain.Rule;
import org.virgodarth.ruleengine.dto.RuleDto;

@Mapper(
    componentModel = "spring",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface RuleMapper {
  Rule ruleDtoToRule(RuleDto ruleDto);

  RuleDto ruleToRuleDto(Rule rule);

  @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
  Rule updateRuleFromRuleDto(RuleDto ruleDto, @MappingTarget Rule rule);
}
