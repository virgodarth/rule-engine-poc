package org.virgodarth.ruleengine.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.virgodarth.ruleengine.domain.Rule;
import org.virgodarth.ruleengine.repository.RuleRepository;
import org.virgodarth.ruleengine.service.RuleService;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class RuleServiceImpl implements RuleService {
  private final RuleRepository ruleRepository;

  @Override
  @Transactional(readOnly = true)
  public List<Rule> getRulesByName(String name) {
    return ruleRepository.findByNamespace(name);
  }
}
