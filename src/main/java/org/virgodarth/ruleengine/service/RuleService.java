package org.virgodarth.ruleengine.service;

import java.util.List;

import org.virgodarth.ruleengine.domain.Rule;

public interface RuleService {
  List<Rule> getRulesByName(String name);
}
