package org.virgodarth.ruleengine.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RuleDto implements Serializable {
  private String code;
  private String namespace;
  private String condition;
  private String action;
  private Integer priority;
  private String description;
}
