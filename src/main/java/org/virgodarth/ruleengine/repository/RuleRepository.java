package org.virgodarth.ruleengine.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.virgodarth.ruleengine.domain.Rule;

@Repository
public interface RuleRepository extends JpaRepository<Rule, Long> {
  List<Rule> findByNamespace(String name);
}
