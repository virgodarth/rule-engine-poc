package org.virgodarth.ruleengine.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.virgodarth.ruleengine.facade.RuleFacade;
import org.virgodarth.ruleengine.facade.ruleengine.impl.loan.UserDetails;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/rules")
public class RuleEngineController {
  private final RuleFacade ruleFacade;

  @GetMapping
  public ResponseEntity<?> getRules(@RequestParam String namespace) {
    return ResponseEntity.ok(ruleFacade.getRulesByNamespace(namespace));
  }

  @PostMapping(value = "/execute")
  public ResponseEntity<?> runLoanRuleEngine(
      @RequestParam String namespace, @RequestBody UserDetails userDetails) {
    return ResponseEntity.ok(ruleFacade.evaluate(namespace, userDetails));
  }
}
